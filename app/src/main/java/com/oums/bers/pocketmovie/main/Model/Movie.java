package com.oums.bers.pocketmovie.main.Model;


import java.io.Serializable;
import java.util.ArrayList;

public class Movie implements Serializable {

    private String title;
    private String id;
    private String voteAverage;
    private String overwiev;
    private String releaseDate;
    private String posterPath;
    private String backdropPath;
    private String primaryKeySql;

    public ArrayList<String> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<String> genres) {
        this.genres = genres;
    }

    private ArrayList<String> genres;


    public Movie() {

    }

    public String getPrimaryKeySql() {
        return primaryKeySql;
    }

    public void setPrimaryKeySql(String primaryKeySql) {
        this.primaryKeySql = primaryKeySql;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public String getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(String voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getOverwiev() {
        return overwiev;
    }

    public void setOverwiev(String overwiev) {
        this.overwiev = overwiev;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }
}
