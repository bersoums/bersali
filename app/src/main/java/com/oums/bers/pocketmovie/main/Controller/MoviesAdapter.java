package com.oums.bers.pocketmovie.main.Controller;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.oums.bers.pocketmovie.main.Model.Movie;
import com.oums.bers.pocketmovie.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Quentin on 18/04/2018.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder>{

    private List<Movie> movies;

    //The listItemCLickListener variable
    final ListItemClickListener listItemClickListener;




    // listItemCLickListener Interface
    public interface ListItemClickListener{
        void onListItemClick(int clickedItemIndex);
    }

    //the constructor of our adapter
    public MoviesAdapter(ListItemClickListener listItemClickListener) {
        this.listItemClickListener = listItemClickListener;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        // get a layoutInflater from the context attached to the parent view
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        // inflate the layout item_layout in a view
        View MovieView = layoutInflater.inflate(R.layout.item_layout,parent,false);

        // create a new ViewHolder with the view articleView
        return new MovieViewHolder(MovieView);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {

        Movie movie = movies.get(position);

        ViewGroup itemViewGroup = (ViewGroup)holder.itemView;

        ImageView imageView=itemViewGroup.findViewById(R.id.imageView_poster);
        //Get image link from Movie Object and converting it with Picasso
        String posterPath=movie.getPosterPath();

        //Resizing the image with the widht and the height of the ImageView and loading it into an ImageView
        Picasso.get().load(posterPath).resize(95,126).into(imageView);

        //Setting the title in the textview
        TextView titleTextView = itemViewGroup.findViewById(R.id.text_item_title);
        titleTextView.setText(movie.getTitle());

        //Setting the average vote in the textview
        TextView idTextView = itemViewGroup.findViewById(R.id.text_item_rating);
        idTextView.setText("Rating : "+movie.getVoteAverage());


    }

    @Override
    public int getItemCount() {
        if (movies != null) {
            return movies.size();
        }
        else {
            return 0;
        }
    }

    public  void setMovies(List<Movie> movies) {
        this.movies = movies;
        this.notifyDataSetChanged();
    }

    class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private MovieViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

        }

        //If an item is clicked it will give the item poisition to the interface
        @Override
        public void onClick(View v) {
            int clickedPosition=this.getAdapterPosition();

            MoviesAdapter.this.listItemClickListener.onListItemClick(clickedPosition);

            Log.i("itemOk", " clicked" );

        }
    }
}
