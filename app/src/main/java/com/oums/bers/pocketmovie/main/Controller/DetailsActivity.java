package com.oums.bers.pocketmovie.main.Controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.oums.bers.pocketmovie.main.Model.AsyncTaskForHttpConnection;
import com.oums.bers.pocketmovie.main.Model.Database;
import com.oums.bers.pocketmovie.main.Model.JsonClass;
import com.oums.bers.pocketmovie.main.Model.Movie;
import com.oums.bers.pocketmovie.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;
import com.squareup.picasso.RequestCreator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import static android.content.res.Configuration.SCREENLAYOUT_SIZE_LARGE;
import static android.content.res.Configuration.SCREENLAYOUT_SIZE_XLARGE;


public class DetailsActivity extends AppCompatActivity {

    TextView title;
    TextView releaseDate;
    TextView genre;
    TextView rating;
    TextView overview;
    ImageView backDrop;
    Button video;
    CheckBox pocket;

    String key;

    Database database;
    Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        //Back Arrow Button
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Getting the intent and from the intent the Movie object
        Intent intent=getIntent();
        movie= (Movie) intent.getSerializableExtra("movie");

        //Views
        title=findViewById(R.id.detail_title);
        releaseDate=findViewById(R.id.release_date);
        genre=findViewById(R.id.genre);
        rating=findViewById(R.id.details_rating);
        overview=findViewById(R.id.overview);
        backDrop=findViewById(R.id.imageView_backdrop);
        video=findViewById(R.id.btn_video);
        pocket=findViewById(R.id.checkbox);



        //Creating an instance of the AsyncTaskForHttpConnection
        AsyncTaskForHttpConnection asyncTaskForHttpConnectionDetails=new AsyncTaskForHttpConnection();
        //The string that will hold the result from Api
        String resultFromApiDetails;
        //URL for the detail http request
        String urlString="https://api.themoviedb.org/3/movie/"+movie.getId()+"?api_key=ea2dcee690e0af8bb04f37aa35b75075";
        try {
            //Retrieving detail data from the http request
            resultFromApiDetails=asyncTaskForHttpConnectionDetails.execute(urlString).get();

            //LOG TEST
            Log.i("cpas",resultFromApiDetails);


            //The null result is beacause of the connection problem (with internet)
            if(resultFromApiDetails==null){

                //If the app encounters a connection problem this AlertDialog will pop up
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Connection failure!")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //Dissmising the dialog
                                dialog.dismiss();

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
            else {

                //Retrieving the nescessary details for the Detail Activity (backdropath , release year, overview)
                movie = JsonClass.JsonToListForDetail(resultFromApiDetails, movie);



                //Setting up the  detail values into the views
                title.setText(movie.getTitle());
                releaseDate.setText(movie.getReleaseDate());
                rating.setText("Rating : "+movie.getVoteAverage());
                overview.setText(movie.getOverwiev());
                //Setting the Overview textview Scrollable in case of long overview text
                overview.setMovementMethod(new ScrollingMovementMethod());

                //Setting the first 2 genres in the textview
                genre.setText(movie.getGenres().get(0)+" "+movie.getGenres().get(1));





                //If the Api doesnt provide a link for the backdrop image it will load an image from the drawable folder
                if(movie.getBackdropPath().equals("https://image.tmdb.org/t/p/w500null")) {


                    String imageFromDrawable = "@drawable/defaultbackdrop";
                    int imageResource = getResources().getIdentifier(imageFromDrawable, null, getPackageName());
                    Drawable res = getResources().getDrawable(imageResource);
                    backDrop.setImageDrawable(res);


                }
                //This will load the backdrop from the api
                else {


                        //Setting up the backdrop image, Get image link from Movie Object and putting it in the backDrop ImageView
                        String backdropPath = movie.getBackdropPath();
                        Picasso.get().load(backdropPath).fit().into(backDrop);



                }






            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }


    //Called when Back (Arrow) Button will be clicked // To return to the MainActivity
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


            //Return to the mainActivity
            Intent intent=new Intent(this, MainActivity.class);
            startActivity(intent);


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();

        //Creating the database instance
        database=new Database(this);

        //To check wheter the movie is in database or not
        Boolean inDatabase=database.CheckIsDataAlreadyInDBorNot("MOVIEID", movie.getId());

        //If the movie is in the database so the checkbox will be pre-checked
        if(inDatabase==true){
            pocket.setChecked(true);
        }
        //Otherwise unChecked
        else {
            pocket.setChecked(false);
        }



        //Listener for the Checkbox to add or remove the movie from the SqlLite database
        pocket.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked==true){
                    //Inserting the line into the database and returns true if inserted
                    boolean b=database.insertMovie(movie.getId(),movie.getTitle(), movie.getVoteAverage(), movie.getPosterPath());
                    Log.i("checkbox", Boolean.toString(b));


                }
                else {
                    database.deleteData(movie.getId());
                    Log.i("checkbox", "not checked");

                }
            }
        });
    }

    //Called when the video button is clicked
    public void onVideoClick(View view) {

        String key=null;

        //Creating an instance of the AsyncTaskForHttpConnection to obtain from the Api the key for the Youtube Url
        AsyncTaskForHttpConnection asyncTaskForHttpConnectionYoutube=new AsyncTaskForHttpConnection();

        //Url to get the link for youtube
        String videoId="https://api.themoviedb.org/3/movie/"+movie.getId()+"/videos?api_key=ea2dcee690e0af8bb04f37aa35b75075";

        //Retrieving video link key part from the http request
        try {

            String resultYoutube = asyncTaskForHttpConnectionYoutube.execute(videoId).get();

            //If the app encounter a connection (internet) problem
            if (resultYoutube==null) {


                //If the app encounters a connection problem this AlertDialog will pop up
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Connection failure!")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //Dismiss the dialog
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
            // connection is Ok
            else {
                //The getTrailerLink method returns the key part of the youtube link from the json String
                key = JsonClass.getTrailerLink(resultYoutube);

                //Starting an intent for Youtube
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" +
                        key));

                startActivity(intent);


            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }



    }
}
