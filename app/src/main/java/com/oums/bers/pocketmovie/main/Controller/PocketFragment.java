package com.oums.bers.pocketmovie.main.Controller;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oums.bers.pocketmovie.R;
import com.oums.bers.pocketmovie.main.Model.Database;
import com.oums.bers.pocketmovie.main.Model.Movie;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */

//Implements the ListItemClickListener interface to obtain the item's position that the user clicked upon
public class PocketFragment extends Fragment implements MoviesAdapter.ListItemClickListener {

    ArrayList<Movie> movies;
    Database database;
    RecyclerView recyclerView;

    public PocketFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pocket, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        //Creating the RecyclerView
        recyclerView = view.findViewById(R.id.pocket_movies);

        //For smooth scrolling
        recyclerView.setNestedScrollingEnabled(false);


    }

    //Setting the adapter in the onStart method because this method is launched every time when we slide between Fragments
    //So it can refresh the RecyclerAdapter if  we added some new movies into Pocket
    @Override
    public void onStart() {
        super.onStart();

        //Creating the database object
        database=new Database(getContext());

        //Converting the database into an ArrayList
        movies= database.databaseToArrayList(getContext());



        // Creating an instance of the adapter of the RecyclerView
        MoviesAdapter moviesAdapter= new MoviesAdapter(this);

        //Passing the ArrayList of Movie objects to the adapter of the RecyclerView
        moviesAdapter.setMovies(movies);
        recyclerView.setAdapter(moviesAdapter);

        // set the layoutManager of the recyclerView
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

    }

    //Called when you click at an item
    //The clickedItemIndex Integer is provided by the RecyclerView Adapter represents position of the item that was clicked
    @Override
    public void onListItemClick(int clickedItemIndex) {
        Log.i("itemClicked ", Integer.toString(clickedItemIndex)+movies.get(clickedItemIndex).getTitle());
        //Starting the DetailsActivity with the intent and passing the Movie object
        Intent intent=new Intent(getContext(),DetailsActivity.class);
        intent.putExtra("movie",movies.get(clickedItemIndex));
        startActivity(intent);
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }

}
