package com.oums.bers.pocketmovie.main.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class Database extends SQLiteOpenHelper {

    // the name of our database
    private static final String DB_NAME = "movies";

    // the version of the database
    private static final int DB_VERSION = 1;


    public Database(Context context) {

        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //Creating the Movies table in SqlLite with all the nescessary fields to save movie objects in the database
        db.execSQL("CREATE TABLE MOVIES ("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "MOVIEID TEXT, "
                + "TITLE TEXT, "
                + "RATING TEXT, "
                + "IMAGE); "

        );


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+DB_NAME);
        onCreate(db);


    }

    public Cursor getAllValues() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+DB_NAME,null);
        return res;
    }

    //Insert a new row in the database and return true if its inserted
    public boolean insertMovie(String movieId,String title, String rating, String image) {

        SQLiteDatabase db=this.getWritableDatabase();

        ContentValues movieValues = new ContentValues();

        movieValues.put("MOVIEID", movieId);
        movieValues.put("TITLE", title);
        movieValues.put("RATING", rating);

        movieValues.put("IMAGE", image);



        //returns -1 if insertion failed
        long result=db.insert("MOVIES", null, movieValues);

        if(result == -1)
            return false;
        else
            return true;
    }


    //Delete a row from the database with the unique primary key and return true if its deleted
    public boolean deleteData(String id)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        return db.delete(DB_NAME, "MOVIEID" + "=" + id, null) > 0;
    }

    //Method that will return true if the field exists in the database
    public boolean CheckIsDataAlreadyInDBorNot(String dbfield, String fieldValue) {
        SQLiteDatabase db=this.getWritableDatabase();
        String Query = "Select * from " + "MOVIES" + " where " + dbfield + " = " + fieldValue;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }




    //Method that will convert the databse into an ArrayList of Person's
    //Returns an ArrayList of Person
    public ArrayList<Movie> databaseToArrayList(Context context){

        //In the while loop the Movie objects will be added to the ArrayList of Movies


        //the ArrayList of Persons that will receive the data (in converted form) from the database
        ArrayList<Movie> movieList=new ArrayList<Movie>();



        Cursor c = getAllValues(); //function to retrieve all values from a table
        int i=0;
        while (c.moveToNext())
        {



            try
            {
                //Getting name , number and the primary key from the database
                String movieId = c.getString(c.getColumnIndex("MOVIEID"));
                String title = c.getString(c.getColumnIndex("TITLE"));
                String rating=c.getString(c.getColumnIndex("RATING"));
                String image=c.getString(c.getColumnIndex("IMAGE"));
                int primary_key=c.getInt(c.getColumnIndex("_id"));




                //Setting the values for the person object
                Movie movie=new Movie();
                movie.setTitle(title);
                movie.setId(movieId);
                movie.setVoteAverage(rating);
                movie.setPosterPath(image);
                movie.setPrimaryKeySql(Integer.toString(primary_key));

                //Adding the Movie object to the ArrayList
                movieList.add(movie);


            }
            //If somethings goes wrong
            catch (Exception e) {
                Log.e("Error", "Error " + e.toString());


            }
            i++;


        }

        c.close();


        //Returning the ArrayList of person Objects
        return movieList;
    }
}
