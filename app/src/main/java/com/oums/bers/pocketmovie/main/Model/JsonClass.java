package com.oums.bers.pocketmovie.main.Model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JsonClass {

    //A Movie object in the parameters is given with some informations already from the Api (from the response with the 20 movies)
    //It will be filled and returned with some additionnal information from the movie detail request
    public static Movie JsonToListForDetail(String jsonString, Movie movie) {


        try {
            JSONObject jsonObject = new JSONObject(jsonString);



                String backdropPath="https://image.tmdb.org/t/p/w500"+jsonObject.getString("backdrop_path");
                String overview=jsonObject.getString("overview");
                String releaseDate=jsonObject.getString("release_date");


                movie.setBackdropPath(backdropPath);
                movie.setOverwiev(overview);
                movie.setReleaseDate(releaseDate);

                //Getting the genre of the movie              //Adding the ArrayList of genre in the Movie object
                movie.setGenres(jsonToGenre(jsonString)); //Returns a String ArrayList containing the genres of the movie



        } catch (JSONException e) {
            Log.e("error ","can't parse json string correctly");
            e.printStackTrace();
        }

        return movie;


    }
    //This method will provide a key part for the link of the trailer in youtube from the Json String received in parameters
    public static String getTrailerLink(String json){

        String key=null;

        try {

            JSONObject jsonObject=new JSONObject(json);

            JSONArray jsonArray=  jsonObject.getJSONArray("results");

            JSONObject jsonObject1= (JSONObject) jsonArray.get(0);



            Log.i("key", jsonObject1.getString("key"));

            key=jsonObject1.getString("key");



        } catch (JSONException e) {
            e.printStackTrace();
        }

        return key;
    }

    //This method will take a json String as parameter and returns the ArrayList of String of the movie genre
    public static ArrayList<String> jsonToGenre(String json) {

        ArrayList<String> arrayListGenre=new ArrayList<String>();

        try {
            //Making a JsonObject of the String received in parameters
            JSONObject jsonObject=new JSONObject(json);

            //Transforming the jsonObject into an JsonArray with genres
            JSONArray jsonArray=jsonObject.getJSONArray("genres");

            //for each genre of the jsonArray , extracting the name of the genre

            for(int i=0;i<jsonArray.length();i++) {
                String s=jsonArray.get(i).toString();
                Log.i("genres ",s);

                JSONObject json2=new JSONObject(s);

                String genreName=json2.getString("name");

                //Log.i("pet ",k);

                //adding the genre name in the String ArrayList
                arrayListGenre.add(genreName);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrayListGenre;

    }

    //Will convert into an ArrayList the response from the Api with the list of movies
    public static ArrayList<Movie> JsonToListForMovie(String jsonString) {

        ArrayList<Movie> movies = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray jsonArticles = jsonObject.getJSONArray("results");

            for (int i = 0; i<jsonArticles.length(); ++i)
            {
                JSONObject jsonMovies = jsonArticles.getJSONObject(i);

                String title = jsonMovies.getString("title");
                String id = jsonMovies.getString("id");
                String averageVote=jsonMovies.getString("vote_average");
                String posterPath="https://image.tmdb.org/t/p/w500"+jsonMovies.getString("poster_path");

                Movie movie = new Movie();

                movie.setTitle(title);
                movie.setId(id);
                movie.setVoteAverage(averageVote);
                movie.setPosterPath(posterPath);

                movies.add(movie);
            }

        } catch (JSONException e) {
            Log.e("error ","can't parse json string correctly");
            e.printStackTrace();
        }

        return movies;


    }
}
