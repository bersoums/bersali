package com.oums.bers.pocketmovie.main.Controller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter{

    //Le nombre de tab
    private int numberOfTabs;

    public PagerAdapter(FragmentManager fm, int numberOfTabs) {
        super(fm);
        this.numberOfTabs=numberOfTabs;
    }

    //Load the corresponding fragment
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MoviesFragment moviesFragment=new MoviesFragment();
                return moviesFragment;

            case 1:
                PocketFragment pocketFragment=new PocketFragment();
                return pocketFragment;

            default:
                return null;
        }


    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
