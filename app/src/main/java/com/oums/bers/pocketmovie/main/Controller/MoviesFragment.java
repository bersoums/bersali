package com.oums.bers.pocketmovie.main.Controller;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.oums.bers.pocketmovie.main.Model.AsyncTaskForHttpConnection;
import com.oums.bers.pocketmovie.main.Model.JsonClass;
import com.oums.bers.pocketmovie.main.Model.Movie;
import com.oums.bers.pocketmovie.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static java.sql.Types.NULL;

/**
 * A simple {@link Fragment} subclass.
 */

//Implements the ListItemClickListener interface to obtain the item's position that the user clicked upon
public class MoviesFragment extends Fragment implements MoviesAdapter.ListItemClickListener{

    ArrayList<Movie> movies;

    int pageNumber;



    //TextView "Next Page" used as a button
    TextView nextPage;


    RecyclerView recyclerView;


    MoviesAdapter moviesAdapter;

    LinearLayoutManager layoutManager;



    public MoviesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movies, container, false);
        
    }

    //Called when you click at an item
    //The clickedItemIndex Integer is provided by the RecyclerView Adapter represents position of the item that was clicked
    @Override
    public void onListItemClick(int clickedItemIndex) {
        Log.i("itemClicked ", Integer.toString(clickedItemIndex)+movies.get(clickedItemIndex).getTitle());
        //Starting the DetailsActivity with the intent and passing the corresponding Movie object
        Intent intent=new Intent(getContext(),DetailsActivity.class);
        intent.putExtra("movie",movies.get(clickedItemIndex));
        startActivity(intent);

    }



    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }



    @Override
    public void onStart() {
        super.onStart();



        //When Next Page is clicked the pageNumber will be incremented and the http request will return new results
        //so the adapter of the recycler view will be updated
        nextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //CQLLED WHEN NEXT PAGE IS CLICKED
                pageNumber++;

                //The url of the Api
                String urlString = "https://api.themoviedb.org/3/movie/popular?page="+pageNumber+"&api_key=ea2dcee690e0af8bb04f37aa35b75075";

                AsyncTaskForHttpConnection asyncTaskForHttpConnection=new AsyncTaskForHttpConnection();

                //Retrieving data from the http request
                String resultFromApi= null;
                try {

                    resultFromApi = asyncTaskForHttpConnection.execute(urlString).get();
                    //LOG TEST
                    Log.i("cpas2",resultFromApi);

                    if (resultFromApi==null) {

                        //If the app encounters a connection problem this AlertDialog will pop up
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setMessage("Connection failure!")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //Dismissing the dialog
                                        dialog.dismiss();

                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();

                    }
                    else {
                        //Converting the String into a List
                        movies = JsonClass.JsonToListForMovie(resultFromApi);


                        //Passing the ArrayList of Movie objects to the adapter of the RecyclerView
                        moviesAdapter.setMovies(movies);
                        recyclerView.setAdapter(moviesAdapter);



                    }


                } catch (InterruptedException e) {

                    Toast.makeText(getActivity(), "Interruption Error!",
                            Toast.LENGTH_LONG).show();

                    e.printStackTrace();
                } catch (ExecutionException e) {

                    Toast.makeText(getActivity(), "Execution Error!",
                            Toast.LENGTH_LONG).show();

                    e.printStackTrace();
                }



            }
        });

        //CALLED AT THE FIRST LOADING OF THE PAGE

        //The url of the Api
        String urlString = "https://api.themoviedb.org/3/movie/popular?page="+pageNumber+"&api_key=ea2dcee690e0af8bb04f37aa35b75075";



        //Creating an instance of the AsyncTaskForHttpConnection
        AsyncTaskForHttpConnection asyncTaskForHttpConnection=new AsyncTaskForHttpConnection();

        //The string that will hold the result from Api
        String resultFromApi;

        //Empty ArrayList of Objects (type Movie) where we will put our data from the httpRequest
        movies=new ArrayList<Movie>();

        //For smooth scrolling
        recyclerView.setNestedScrollingEnabled(false);


        // Creating an instance of the adapter of the RecyclerView
        moviesAdapter= new MoviesAdapter(this);

        try {



            //Retrieving data from the http request
            resultFromApi=asyncTaskForHttpConnection.execute(urlString).get();



            //If the app encounter a connection (internet) problem
            if (resultFromApi==null) {


                //If the app encounters a connection problem this AlertDialog will pop up
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Connection failure!")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //Dissmising the dialog
                                dialog.dismiss();

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
            // connection is Ok
            else {



                //Converting the String into a List
                movies = JsonClass.JsonToListForMovie(resultFromApi);

                //Passing the ArrayList of Movie objects to the adapter of the RecyclerView
                moviesAdapter.setMovies(movies);

                recyclerView.setAdapter(moviesAdapter);




            }


        } catch (InterruptedException e) {



            e.printStackTrace();
        } catch (ExecutionException e) {



            e.printStackTrace();
        }




    }

    @Override
    public void onViewCreated(View view,  final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




        //Initializing our TextView and the setting the onClickListener on it, this TextView wil serve like a Button
        nextPage=(TextView) view.findViewById(R.id.footer);


        //Creating the RecyclerView
        recyclerView = view.findViewById(R.id.list_articles);

        recyclerView.setAdapter(moviesAdapter);

        // set the layoutManager of the recyclerView
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);


        //Initializing the page number at 1
        pageNumber=1;




    }
}
